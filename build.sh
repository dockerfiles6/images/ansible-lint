#!/usr/bin/env sh
export PRODUCT=ansible-lint
export SOURCE=ansible-community/ansible-lint

pip install lastversion --user

export VERSION=$(lastversion ${SOURCE})

export DOCKER_BUILDKIT=1

docker build --build-arg VERSION=${VERSION} -t ${REGISTRY}/${PRODUCT}:${VERSION} .
