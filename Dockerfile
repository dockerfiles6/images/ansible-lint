# syntax=docker/dockerfile:1.3-labs
FROM alpine:3.17 as builder
ENV PYROOT=/venv
ENV PYTHONUSERBASE=$PYROOT
WORKDIR /
COPY Pipfile ./
RUN <<EOF
  apk update
  apk add --no-cache bc cargo gcc libffi-dev musl-dev openssl-dev rust python3-dev py3-pip
  pip3 install --no-cache-dir --no-compile pipenv==2021.5.29
  pipenv lock && PIP_USER=1 pipenv sync --system
EOF

FROM alpine:3.17 as default
COPY --from=builder /venv /venv
COPY --from=builder /usr/lib/python3.10 /usr/lib/python3.10
RUN <<EOF
  apk add --no-cache git python3
  echo export PYTHONPATH="/usr/lib/python3.10:/venv/lib/python$(python3 -c 'import sys; print(".".join(map(str, sys.version_info[:2])))')/site-packages/">>/etc/profile
  echo PATH="/venv/bin:$PATH">>/etc/profile
EOF

# Make sure we use the virtualenv:
WORKDIR /src
COPY entrypoint.sh /usr/bin
ENTRYPOINT ["entrypoint.sh"]
CMD ["--version"]
